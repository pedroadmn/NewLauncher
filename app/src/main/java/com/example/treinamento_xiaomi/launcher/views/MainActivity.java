package com.example.treinamento_xiaomi.launcher.views;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.treinamento_xiaomi.launcher.R;
import com.example.treinamento_xiaomi.launcher.adapter.*;
import com.example.treinamento_xiaomi.launcher.fragments.*;
import com.example.treinamento_xiaomi.launcher.model.Pac;
import com.example.treinamento_xiaomi.launcher.utils.CustomGridLayoutManager;
import com.example.treinamento_xiaomi.launcher.utils.PrefManager;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class MainActivity extends AppCompatActivity {

    private PrefManager prefManager;
    private PackageManager pm;
    private List<Pac> homePacs;
    private DrawerAdapter drawerAdapterObject;
    private CircleIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pm = getPackageManager();


        prefManager = new PrefManager(this);
        if (prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
        }

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        setupViewPager(viewPager);
        setPacHome();
    }

    private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(MainActivity.this, SplashScreenActivity.class));
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter vpAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        vpAdapter.addFragment(new FirstPageFragment());
        vpAdapter.addFragment(new SecondPageFragment());
        vpAdapter.addFragment(new ThirdPageFragment());
        viewPager.setAdapter(vpAdapter);
        viewPager.setCurrentItem(1);
        indicator.setViewPager(viewPager);
    }

    public void setPacHome(){
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> pacList = pm.queryIntentActivities(mainIntent, 0);
        homePacs = new ArrayList<>();
        String label;
        for(ResolveInfo ri : pacList){
            label = ri.activityInfo.packageName;
            Log.d("LABELXIAOMI", label);
            if(label.equals("com.android.browser") || label.equals("com.android.contacts") ||
                    label.equals("com.android.mms") || label.equals("com.android.dialer")){
                Pac pac = new Pac();
                pac.icon = ri.activityInfo.loadIcon(pm);
                pac.label = ri.loadLabel(pm).toString();
                pac.packageName = ri.activityInfo.packageName;
                pac.name = ri.activityInfo.name;
                homePacs.add(pac);
            }
        }

        Pac pac = new Pac();
        pac.icon = ResourcesCompat.getDrawable(getResources(), R.drawable.all_apps, null);
        pac.label = "Apps";
        pac.packageName = "com.example.treinamento_xiaomi.launcher";
        pac.name = "com.example.treinamento_xiaomi.launcher.views.AllAppsActivity";

        homePacs.add(2,pac);

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        CustomGridLayoutManager cgLayoutManager =
                new CustomGridLayoutManager(getApplicationContext(), 5);
        recyclerView.setLayoutManager(cgLayoutManager);

        drawerAdapterObject = new DrawerAdapter(this, homePacs);
        recyclerView.setAdapter(drawerAdapterObject);

    }



    @Override
    public void onBackPressed() {

    }
}
