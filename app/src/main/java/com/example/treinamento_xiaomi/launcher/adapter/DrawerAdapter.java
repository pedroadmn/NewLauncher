package com.example.treinamento_xiaomi.launcher.adapter;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.example.treinamento_xiaomi.launcher.R;
import com.example.treinamento_xiaomi.launcher.fragments.ThirdPageFragment;
import com.example.treinamento_xiaomi.launcher.listener.*;
import com.example.treinamento_xiaomi.launcher.model.Pac;
import com.example.treinamento_xiaomi.launcher.utils.Util;

import java.util.List;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {

    private List<Pac>  pacsAdapter;
    private Context context;

    public DrawerAdapter(Context context, List<Pac> pacs) {
        this.pacsAdapter = pacs;
        this.context = context;
    }

    @Override
    public DrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.drawer_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DrawerAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.icon_text.setText(pacsAdapter.get(i).label);
        viewHolder.icon_image.setImageDrawable(pacsAdapter.get(i).icon);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent launchIntent = new Intent(Intent.ACTION_MAIN);
                launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                ComponentName cp = new ComponentName(pacsAdapter.get(i).packageName, pacsAdapter.get(i).name);
                launchIntent.setComponent(cp);
                context.startActivity(launchIntent);
            }
        });

        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (Util.copiedAppList.contains(pacsAdapter.get(i).label)){
                    return true;
                } else {
                    LayoutParams lp = new LayoutParams(viewHolder.itemView.getWidth(), viewHolder.itemView.getHeight());
                    if((int) viewHolder.itemView.getY() > 800){
                        lp.leftMargin = (int) viewHolder.itemView.getX();
                        lp.topMargin = 765 ;
                    } else {
                        lp.leftMargin = (int) viewHolder.itemView.getX();
                        lp.topMargin = (int) viewHolder.itemView.getY();
                    }
                    LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    LinearLayout ll = (LinearLayout) li.inflate(R.layout.drawer_item, null);
                    ((ImageView)ll.findViewById(R.id.icon_image)).setImageDrawable(pacsAdapter.get(i).icon);
                    ((TextView)ll.findViewById(R.id.icon_text)).setText(pacsAdapter.get(i).label);

                    ll.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            view.setOnTouchListener(new AppTouchListener());
                            return true;
                        }
                    });

                    ll.setOnClickListener(new AppClickListener(pacsAdapter, context));
                    String[] data = new String[2];
                    data[0] = pacsAdapter.get(i).packageName;
                    data[1] = pacsAdapter.get(i).name;
                    ll.setTag(data);
                    Util.copiedAppList.add(pacsAdapter.get(i).label);
                    ThirdPageFragment.rl.addView(ll, lp);
                    String path = ((context).toString()).substring(0, ((context).toString()).length() - 9);
                    if ((path.equals("com.example.treinamento_xiaomi.launcher.views.AllAppsActivity"))){
                        ((Activity)context).finish();
                    }
                    return true;}
            }
        });
    }

    @Override
    public int getItemCount() {
        return pacsAdapter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView icon_text;
        private ImageView icon_image;

        public ViewHolder(View view) {
            super(view);
            icon_text = (TextView)view.findViewById(R.id.icon_text);
            icon_image = (ImageView) view.findViewById(R.id.icon_image);
        }
    }
}
