package com.example.treinamento_xiaomi.launcher.fragments;

import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetHostView;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.example.treinamento_xiaomi.launcher.R;
import com.example.treinamento_xiaomi.launcher.adapter.DrawerAdapter;
import com.example.treinamento_xiaomi.launcher.model.Pac;
import com.example.treinamento_xiaomi.launcher.utils.CustomGridLayoutManager;
import com.example.treinamento_xiaomi.launcher.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class SecondPageFragment extends Fragment {

    private ImageView imgMiuiTheme;
    private PackageManager pm;
    private List<Pac> MIUIPacs;
    private RecyclerView recyclerView;
    private DrawerAdapter drawerAdapterObject;

    public SecondPageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pm = getActivity().getPackageManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inf = inflater.inflate(R.layout.second_page_fragment, container, false);
        imgMiuiTheme = (ImageView) inf.findViewById(R.id.iv_miui_theme);
        recyclerView = (RecyclerView) inf.findViewById(R.id.gridview_secondpage);

        imgMiuiTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Util.URI));
                intent.setPackage("com.android.browser");
                startActivity(intent);
            }
        });

        setPacMIUI();

        return inf;
    }

    private void setPacMIUI(){
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> pacList = pm.queryIntentActivities(mainIntent, 0);
        MIUIPacs = new ArrayList<>();
        String label;
        for(ResolveInfo ri : pacList){
            label = ri.activityInfo.packageName;
            if(label.contains("miui")){
                Pac pac = new Pac();
                pac.icon = ri.activityInfo.loadIcon(pm);
                pac.label = ri.loadLabel(pm).toString();
                pac.packageName = ri.activityInfo.packageName;
                pac.name = ri.activityInfo.name;
                MIUIPacs.add(pac);
            }
        }

        CustomGridLayoutManager cgLayoutManager =
                new CustomGridLayoutManager(getContext(), 4);

        recyclerView.setLayoutManager(cgLayoutManager);
        drawerAdapterObject = new DrawerAdapter(getContext(), MIUIPacs);
        recyclerView.setAdapter(drawerAdapterObject);

    }
}
