package com.example.treinamento_xiaomi.launcher.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.treinamento_xiaomi.launcher.R;
import com.example.treinamento_xiaomi.launcher.utils.ReadRss;


public class NewsFragment extends Fragment {

    RecyclerView recyclerView;

    public NewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.news_fragment, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_news);
        ReadRss readRss = new ReadRss(view.getContext(), recyclerView);
        readRss.execute();

        return view;
    }

}