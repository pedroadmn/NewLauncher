package com.example.treinamento_xiaomi.launcher.listener;

import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;

public class AppTouchListener implements View.OnTouchListener {

    int leftMargine;
    int topMargine;

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()){
            case MotionEvent.ACTION_MOVE:
                LayoutParams lp = new LayoutParams(view.getWidth(), view.getHeight());
                leftMargine = (int) motionEvent.getRawX() - view.getWidth()/2;
                topMargine = (int) motionEvent.getRawY() - view.getHeight()/2;

                if (leftMargine + view.getWidth() > view.getRootView().getWidth()){
                    leftMargine = view.getRootView().getWidth() - view.getWidth();
                }

                if (leftMargine < 0){
                    leftMargine = 0;
                }

                if (topMargine + view.getHeight() > ((View) view.getParent()).getHeight()){
                    topMargine = ((View) view.getParent()).getHeight() - view.getHeight();
                }

                if (topMargine < 0){
                    topMargine = 0;
                }

                lp.leftMargin = leftMargine;
                lp.topMargin = topMargine;
                view.setLayoutParams(lp);
                break;
            case MotionEvent.ACTION_UP:
                view.setOnTouchListener(null);
                break;
        }
        return false;
    }
}
