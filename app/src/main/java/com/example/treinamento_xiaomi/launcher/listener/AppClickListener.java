package com.example.treinamento_xiaomi.launcher.listener;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.example.treinamento_xiaomi.launcher.model.Pac;

import java.util.List;

public class AppClickListener implements View.OnClickListener {

    private Context context;

    public AppClickListener(List<Pac> pacs, Context cont){
        context = cont;
    }

    @Override
    public void onClick(View view) {
        String[] data;
        data = (String[]) view.getTag();
        Intent launchIntent = new Intent(Intent.ACTION_MAIN);
        launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        ComponentName cp = new ComponentName(data[0],data[1]);
        launchIntent.setComponent(cp);
        context.startActivity(launchIntent);
    }
}
